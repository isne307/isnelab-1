#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	//Sample Code
	List mylist;

	cout<<"pushToHead:"<<endl;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');

	mylist.print();
	cout<<endl;

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list,

	cout<<"\npushToTail:"<<endl;
	mylist.pushToTail('e');
	mylist.pushToTail('r');
	mylist.pushToTail('n');

    mylist.print();
	cout<<endl;

	cout<<"\npopHead:"<<endl;
	mylist.popHead();

	mylist.print();
	cout<<endl;

	cout<<"\npopTail:"<<endl;
	mylist.popTail();

    mylist.print();
	cout<<endl;

	cout<<"\nSearch:"<<endl;
    cout<<mylist.search('z');
    cout<<mylist.search('a');
    cout<<mylist.search('k');
    cout<<mylist.search('r');
    cout<<endl;

    //- then reverse the list to see if it is a palindrome!
    cout<<"\nBefore:"<<endl;
    mylist.print();
    cout<<endl;

    cout<<"\nReverse:"<<endl;
    mylist.reverse();
	mylist.print();
	cout<<endl;



}
