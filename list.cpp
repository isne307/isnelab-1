#include <iostream>
#include "list.h"

using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if(tail==0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	//TO DO!
	Node* pt = new Node(el, 0);
	if(head==0) {
        head = pt;
        tail = head;
	}
	else
	{
        tail -> next = pt;
        tail = tail -> next;
	}
    delete pt;
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	 // TO DO!
	 if(head == tail)
     {
         cout<<tail->data;
         head = tail = 0;
     }
     else
     {
         Node* tmp = head;
         while(tmp->next != tail)
         {
             tmp = tmp -> next;
         }
         delete tail;
         tail = tmp;
         tail -> next = 0;
         delete tmp;
     }

	 return false;
}
bool List::search(char el)
{
	// TO DO! (Function to return True or False depending if a character is in the list.
    if(tail == 0)
    {
        return false;
    }
    Node* sh = head;
    while(sh != 0)
    {
    if(el == sh->data)
        {
            return true;
        }
        sh = sh->next;
    }
    return NULL;
}
void List::reverse()
{
    // TO DO! (Function is to reverse the order of elements in the list.
    if(isEmpty())
    {
        return;
    }
    tail = head;
    Node *prev = 0;
    Node *curr = head;
    while(curr)
    {
        Node *next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }
    head = prev;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
